package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static final String CONNECTION_URL = "jdbc:mysql://localhost:3306/testdb" +
			"?user=zodin&password=admin";

	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if(instance == null)
			instance = new DBManager();
		return instance;
	}

	private DBManager() {
	}

	public Connection getConnection()throws SQLException{
		Properties properties = new Properties();
		try(InputStream inputStream =
				new FileInputStream("app.properties")){
			properties.load(inputStream);
		}catch(IOException e){
			e.printStackTrace();
		}
		return DriverManager.getConnection(properties.getProperty("connection.url"));
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();

		try {
			Connection connection = getConnection();
			PreparedStatement preparedStatement =
					connection.prepareStatement("SELECT * from users");
			preparedStatement.executeQuery();

			ResultSet rs = preparedStatement.getResultSet();

			while(rs.next()){
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setLogin(rs.getString("login"));
				users.add(user);
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		try{
			Connection connection = getConnection();
			PreparedStatement preparedStatement =
					connection.prepareStatement("INSERT INTO users VALUES (default,?)");
			preparedStatement.setString(1, user.getLogin());
			preparedStatement.executeUpdate();
		}catch (SQLException e){
			e.printStackTrace();
		}
		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		return true;
	}

	public User getUser(String login) throws DBException {
		User user = new User();

		try{
			Connection connection = getConnection();
			PreparedStatement preparedStatement =
					connection.prepareStatement("SELECT * from users WHERE login = ?");
			preparedStatement.setString(1, login);
			preparedStatement.executeQuery();
			ResultSet rs= preparedStatement.getResultSet();
			while(rs.next()){
				user.setLogin(rs.getString("login"));
				user.setId(rs.getInt("id"));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}

		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = new Team();

		try{
			Connection connection = getConnection();
			PreparedStatement preparedStatement =
					connection.prepareStatement("SELECT * FROM teams WHERE name =?");
			preparedStatement.setString(1, name);
			preparedStatement.executeQuery();

			ResultSet rs = preparedStatement.getResultSet();

			while(rs.next()){
				team.setId(rs.getInt("id"));
				team.setName(rs.getString("name"));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}

		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();

		try{
			Connection connection = getConnection();
			PreparedStatement preparedStatement =
					connection.prepareStatement("SELECT * FROM teams");
			preparedStatement.executeQuery();

			ResultSet rs = preparedStatement.getResultSet();

			while(rs.next()){
				Team team = new Team();
				team.setName(rs.getString("name"));
				team.setId(rs.getInt("id"));
				teams.add(team);
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		try{
			Connection connection = getConnection();
			PreparedStatement preparedStatement =
					connection.prepareStatement("INSERT INTO teams VALUES (default,?)");
			preparedStatement.setString(1, team.getName());
			preparedStatement.executeUpdate();
			team.setId(getTeam(team.getName()).getId());
		}catch (SQLException e){
			e.printStackTrace();
		}
		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection connection;
		try{
			connection = getConnection();
			try {
				connection.setAutoCommit(false);

				PreparedStatement preparedStatement =
						connection.prepareStatement("INSERT INTO users_teams VALUES (?,?)");

				for (Team team : teams) {
					preparedStatement.setInt(1, getUser(user.getLogin()).getId());
					preparedStatement.setInt(2, getTeam(team.getName()).getId());

					preparedStatement.executeUpdate();
				}
			}catch (SQLException e){
				try{
					connection.rollback();
					throw new DBException(e.getMessage(), e);
				}catch (SQLException t){
					throw new DBException(t.getMessage(), t);
				}
			}finally {
				connection.commit();
			}
		} catch(SQLException e){
			e.printStackTrace();
		}
		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teamsRes = new ArrayList<>();
		List<Team> teams = findAllTeams();
		try{
			Connection connection = getConnection();
			PreparedStatement preparedStatement =
					connection.prepareStatement("SELECT * from users_teams WHERE user_id =?");
			preparedStatement.setInt(1, getUser(user.getLogin()).getId());

			preparedStatement.executeQuery();
			ResultSet rs = preparedStatement.getResultSet();
			while(rs.next()){
				for(Team team: teams)
					if(team.getId() == rs.getInt("team_id"))
						teamsRes.add(team);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return teamsRes;
	}

	public boolean deleteTeam(Team team) throws DBException {
		try{
			Connection connection = getConnection();

			PreparedStatement preparedStatement =
					connection.prepareStatement("DELETE FROM teams WHERE id=?");
			preparedStatement.setInt(1, getTeam(team.getName()).getId());

			preparedStatement.executeUpdate();
		}catch (SQLException e){
			e.printStackTrace();
		}

		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		try {
			Connection connection = getConnection();
			PreparedStatement preparedStatement =
					connection.prepareStatement("UPDATE teams SET name = ? WHERE id = ?");
			preparedStatement.setString(1, team.getName());
			preparedStatement.setInt(2, team.getId());
			preparedStatement.executeUpdate();
		}catch (SQLException e){
			e.printStackTrace();
		}

		return true;
	}

}
